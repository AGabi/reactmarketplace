import {
  FETCH_CHUNK_OF_PRODUCTS,
  FETCH_PRODUCTS,
  SORT_PRODUCTS,
} from "../types";

export const fetchProducts = () => async (dispatch) => {
  const res = await fetch("/api/products");
  const data = await res.json();
  dispatch({
    type: FETCH_PRODUCTS,
    payload: data,
  });
};
export const sortProducts = (products, sortValue) => (dispatch) => {
  dispatch({
    type: SORT_PRODUCTS,
    payload: {
      sortValue: sortValue,
      items: products
        .slice()
        .sort((a, b) =>
          sortValue === "lowest"
            ? a.price >= b.price
              ? 1
              : -1
            : sortValue === "highest"
            ? a.price <= b.price
              ? 1
              : -1
            : a.year <= b.year
            ? 1
            : -1
        ),
    },
  });
};
export const showProducts = (products, chunkSize) => (dispatch) => {
  dispatch({
    type: FETCH_CHUNK_OF_PRODUCTS,
    payload: {
      chunkSize: chunkSize,
      items: products.slice(chunkSize),
    },
  });
};
