import React, { Component } from "react";
import { connect } from "react-redux";
import { showProducts, sortProducts } from "../actions/productActions";

class Filter extends Component {
  render() {
    return !this.props.filteredProducts ? (
      <div>Loading...</div>
    ) : (
      <div className="filter">
        <div className="filter-result">
          {this.props.filteredProducts.length} Products
        </div>
        <div className="filter-sort">
          Order
          <select
            value={this.props.sort}
            onChange={(e) =>
              this.props.sortProducts(this.props.products, e.target.value)
            }
          >
            <option>Latest</option>
            <option value="lowest">Lowest</option>
            <option value="highest">Highest</option>
          </select>
        </div>
        <div className="filter-show">
          Show
          <select
            value={this.props.size}
            onChange={(e) =>
              this.props.showProducts(this.props.products, e.target.value)
            }
          >
            <option value="">All</option>
            <option value="10">10</option>
            <option value="25">25</option>
            <option value="50">50</option>
            <option value="100">100</option>
          </select>
        </div>
      </div>
    );
  }
}

export default connect(
  (state) => ({
    sortValue: state.products.sort,
    chunkSize: state.products.size,
    filteredProducts:
      state.products
        .filteredItems /*inside productReducer filteredItems is set up to payload*/,
    products:
      state.products.items /*inside productReducer items is set up to payload*/,
  }),
  {
    showProducts,
    sortProducts,
  }
)(Filter);
